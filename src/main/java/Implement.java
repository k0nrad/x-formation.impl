/**
* Initialises the field
*
* A mine-field of N x M squares is represented by N lines of exactly M characters each.
* The character '*' represents a mine * and the character '.' represents no-mine.
* Lines are separated by '\n'
* <p/>
* * Example mine-field string (as input to setMineField()): "*...\n..*.\n...."
* (a 3 x 4 mine-field of 12 squares, 2 of which are mines)
* 
* @param mineField string containing the mines
* @throws IllegalArgumentException if mineField is not properly formatted
*/
//void setMineField(String mineField) throws IllegalArgumentException;
/**
* Produces a hint-field of identical dimensions as the mineFiled() where each
* square is a * for a mine or the number of adjacent mine-squares if the square does
not contain a mine.
* <p/>
* Example hint-field (for the above input): "*211\n12*1\n0111"
*
* @return a string representation of the hint-field
* @throws IllegalStateException if the mine-field has not been initialised (i.e.
setMineField() has not been called)
*/
/*String getHintField() throws IllegalStateException;
}*/




public class Implement {
	private char[][] charArray;
	private int len1;
	private int len2;
	
	public static void main (String[] args) {
		Implement implement = new Implement();
		implement.setMineField("*....\n...*.\n....*");
		String res = implement.getHintField();
		System.out.println("Wynik " + "\n" + res);
		
	}
	
	
	void setMineField(String mineField) throws IllegalArgumentException{
		String[] separated = mineField.split("\n");
		len1 = separated[0].length();
		len2 = separated.length;
		charArray = new char[len1][len2];
		
		for(int i = 1; i<len2 ; i++)
		{
			if(separated[0] != separated[i]) {
				throw new IllegalArgumentException("Wrong size, it's not a rectangle");
			}
		}
		
		int i =0;
		for (String string : separated) {
			charArray[i] = string.toCharArray();
			i++;
		}
		for(int k =0; k< len2; k++) {
			for(int j =0; j< len1; j++) {
				System.out.print(charArray[k][j]);
			}
			System.out.println();
		}
		
		
	}
	String getHintField() throws IllegalStateException {
		
	 for (int i = 0; i < len2; i++)
	    {
	        for (int j = 0; j < len1; j++)
	        {
	            if ((charArray[i][j] == '.'))
	            {
	                int count = 0;                    
	                for (int x = i -1; x <= i + 1; x++)
	                {          
	                	for (int y = j - 1; y <= j + 1; y++)
	                    {
	                        if (0 <= x && x < len2 && 0 <= y && y < len1)
	                        {
	                            if (charArray[x][y] == '*')
	                                count++;
	                        } 
	                    } 
	                } 
	                char temp = Character.forDigit(count,10 );
	                charArray[i][j] = temp;
	                
	            } 

	        } 
	    }
	
	 StringBuilder builder = new StringBuilder();
	    for(int i = 0; i < len2 ; i++)
	    {
	        for(int j = 0; j < len1; j++)
	        {
	            builder.append(charArray[i][j]);
	        }
	        builder.append("\n");
	    }    
	    return builder.toString();
	
	}

}
